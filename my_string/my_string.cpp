﻿#include "my_string.h"

my_string::my_string(const char* that) {
	int i = 0;
	for (; that[i] != '\0'; i++) { }
	
	_count = i;
	char* new_ptr = new char[_count + 1];

	i = 0;
	while (true) {
		new_ptr[i] = that[i];
		if (new_ptr[i] == '\0') break;
		i++;
	}

	ptr = new_ptr;
}

my_string::my_string(const my_string& that) {
	char* new_ptr = new char[that._count+1];

	int i = 0;
	while (true) {
		new_ptr[i] = that[i];
		if (new_ptr[i] == '\0') break;
		i++;
	}

	_count = that._count;
	ptr = new_ptr;
}

my_string::my_string(my_string&& that) {
	ptr = that.ptr;
	_count = that._count;
	that.ptr = nullptr;
}

my_string& my_string::operator= (my_string&& that) {
	const char* buffer = ptr;
	ptr = that.ptr;
	that.ptr = buffer;

	_count = that._count;
	return *this;
}

my_string& my_string::operator= (const my_string& that) {
	my_string* new_str = new my_string(that);
	return *new_str;
}

int my_string::get_count() {
	return _count;
}

my_string& my_string::operator+ (const my_string& that) const {
	char* new_ptr = new char[that._count + _count+1];
	int i = 0;
	for (; i < _count; i++) {
		new_ptr[i] = ptr[i];
	}
	for (; i < that._count + _count+1; i++) { //+1 чтобы в конце добавило '\0'
		new_ptr[i] = that[i - _count];
	}

	return *new my_string(new_ptr);
}

std::ostream& operator<< (std::ostream& os, my_string str) {
	for (int i = 0; i < str._count; i++) {
		os << str.ptr[i];
	}
	return os;
}

const char my_string::operator[] (int index) const {
	return ptr[index];
}

bool my_string::back_lexicographic_comp(my_string str1, my_string str2) {
	int min_count = 
		(str1._count <= str2._count) ? str1._count : str2._count;

	for (int i = 0; i < min_count; i++) {
		if (tolower(str1[i]) < tolower(str2[i])) {
			return false;
		}
	}
	return (str1._count >= str2._count) ? true : false;
}