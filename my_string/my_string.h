#pragma once
#include <iostream>

class my_string {
public:
	my_string(const char* that);
	my_string(const my_string& that);
	my_string(my_string&& that);
	my_string& operator= (const my_string& that);
	my_string& operator= (my_string&& that);
	my_string& operator+ (const my_string& that) const;
	const char operator[] (int index) const;
	friend std::ostream& operator<< (std::ostream& os, my_string str);
	int get_count();
	
	static bool back_lexicographic_comp (my_string str1, my_string str2);

	~my_string() { delete(ptr); }
private:
	const char* ptr;
	int _count;
};

/*����������� ����� ������������ �����, �������:
1. ������� �� C++11 ��� ����� ���� ��������� ���������.�� ���������� � ����� ������
����� - ���� ������ ���������� ������������ �����, ����������� ��� ����� ����������
STL.

2. ����� �� ���������.������������ ������ ������������� �������, ���������
�������
�������� ������ � ������������ �����(������� malloc() / free() ��� ���������
new / delete)
��� ����������� �������������������� �������� ���� char.

3. ����� ����������� �����������, �������� ������������ � ������������
move - ���������.
� ������ ���� "�������" �������� �������� ���� �����.
��� ������������� ������������ ����� - �� ���������� ������ �����������(���� COW � ������������� ���������).

�������� ������ � �������������� ����� ������ ����� � ������ - ���� ���������� STL.

��������� ������ ��������� ������ �����, ����� �������� ��� � �������, ��������
������������������� ��� ����� ��������.
��������� - ������ �������� �� ��������� ������.*/