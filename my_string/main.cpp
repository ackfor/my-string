#include <iostream>
#include <vector>
#include <algorithm>
#include <Windows.h>
#include "my_string.h"

using namespace std;

int main()
{
    vector<my_string> arr;

    arr.push_back(my_string("a"));
    arr.push_back(my_string("q"));
    arr.push_back(my_string("qw"));
    arr.push_back(my_string("qwe"));
    arr.push_back(my_string("Qwer"));

    sort(arr.begin(), arr.end(), my_string::back_lexicographic_comp);

    for (int i = 0; i < arr.size(); i++) {
        cout << arr[i] << '\n';
    }

    std::system("pause");

    delete[](&arr);
}